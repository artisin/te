var gulp         = require('gulp'),
    stylus       = require('gulp-stylus'),
    handleErrors = require('../lib/handleErrors'),
    config       = require('../config/stylus');

gulp.task('stylus', function () {
  var te = require('../../lib/te');
  return gulp.src(config.src)
  //Only complie changes
  .pipe(stylus({
    use: [te()]
  }))
  .on('error', handleErrors)
  .pipe(gulp.dest(config.dest));
});

