var gulp          = require('gulp'),
    webpack       = require('webpack'),
    path          = require('path'),
    nodemon       = require('nodemon'),
    watch         = require('gulp-watch'),
    paths         = require('../config/index'),
    stylus        = require('../config/stylus'),
    jsDest        = paths.publicAssets;

//Default
gulp.task('default', ['run']);

/*
Common
 */
gulp.task('watch', ['backend-watch']);
function onBuild(done) {
  return function(err, stats) {
    if (err) {
      console.log('Error', err);
    }
    console.log(stats.toString());
    if (done) {
      done();
    }
  };
}

/*
Watch stylus files
 */
gulp.task('stylus-watch', function() {
  watch(stylus.src, function() { gulp.start('stylus'); });
});


/*
Back end
 */
gulp.task('backend-watch', function() {
  var config = require('../config/webpack')('dev');
  var backendConfig = config.backend;
  webpack(backendConfig).watch(100, function(err, stats) {
    onBuild()(err, stats);
    nodemon.restart();
  });
});

/*
Runner
 */
gulp.task('run', ['backend-watch'], function() {
  nodemon({
    execMap: {
      js: 'node'
    },
    script: path.join(jsDest, '/te'),
    ignore: ['*'],
    ext: 'noop'
  }).on('restart', function() {
    console.log('Restarted!');
  });
});