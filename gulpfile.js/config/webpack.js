var path             = require('path'),
    paths            = require('./'),
    deepExtend       = require('deep-extend'),
    webpack          = require('webpack'),
    fs               = require('fs');


module.exports = function(env) {
  var jsSrc = path.resolve(paths.sourceAssets),
      jsDest = paths.publicAssets,
      publicPath = './';


  /*
  Common
   */
  var defaultConfig = {
    module: {
      loaders: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loaders: ['babel']
        }
      ]
    }
  };
  var config = function(overrides) {
    return deepExtend(defaultConfig, overrides || {});
  };

  /*
  Enviroment
   */
  if (env !== 'production') {
    defaultConfig.devtool = '#eval-source-map';
    defaultConfig.debug = true;
  }


  /*
  Back End Config
   */
  var nodeModules = {};
  fs.readdirSync('node_modules')
    .filter(function(x) {
      return ['.bin'].indexOf(x) === -1;
    })
    .forEach(function(mod) {
      nodeModules[mod] = 'commonjs ' + mod;
    });

  var backend = config({
    entry: './src/te.js',
    target: 'node',
    output: {
      path: jsDest,
      filename: 'te.js',
      library: 'te',
      libraryTarget: 'commonjs2'
    },
    node: {
      __dirname: true,
      __filename: true
    },
    externals: nodeModules
  });

  return {
    backend: backend
  };
};
