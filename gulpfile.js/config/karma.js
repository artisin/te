var config        = require('./'),
    te            = require('../../lib/te'),
    webpackConfig = require('./webpack-stylus')('test');

module.exports = {
  frameworks: ['mocha', 'sinon-chai'],
  files: [
    config.testDirectory + '/**/*.js',
    config.testDirectory + '/**/*.styl',
    config.testDirectory + '/**/*.css'
  ],
  exclude: [
    config.testDirectory + '/**/*.txt'
  ],
  watch: [
    //Watch for txt desc to run test
    config.testDirectory + '/**/*.txt'
  ],
  preprocessors: {
    '__tests__/*': ['webpack', 'sourcemap'],
    '__tests__/**/*.styl': ['stylus']
  },
  stylusPreprocessor: {
    options: {
      paths: ['lib'],
      use: [te()],
      save: true
    },
    transformPath: function(path) {
      path = path.replace(/\.styl$/, '.compiled.css');
      path = path.replace(/\/cases\//g, '/cases/COMPILED/');
      return path;
    }
  },
  webpack: webpackConfig,
  //Bumped up to comp for buttron complie time
  browserNoActivityTimeout: 100000,
  singleRun: true,
  reporters: ['nyan'],
  colors: true,
  browsers: ['PhantomJS_custom'],
    // you can define custom flags
    customLaunchers: {
      'PhantomJS_custom': {
        base: 'PhantomJS',
        options: {
          windowName: 'my-window',
          settings: {
            webSecurityEnabled: false
          },
        },
        flags: ['--load-images=false'],
        debug: false
      }
    },

    phantomjsLauncher: {
      // Have phantomjs exit if a ResourceError is encountered (useful if karma exits without killing phantom)
      exitOnResourceError: true
    }
};