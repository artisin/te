var path            = require('path'),
    paths           = require('./'),
    webpack         = require('webpack');

module.exports = function() {
  var jsSrc = path.resolve(paths.testDirectory + '/');
  var webpackConfig = {
    context: jsSrc,
    plugins: [],
    resolve: {
      extensions: ['', '.js', '.css', '.txt']
    },
    module: {
      loaders: [
        {
          test: /\.js?$/,
          loader: 'babel',
          exclude: /(node_modules|bower_components)/
        },
        {
          test: /\.css$/,
          loader: 'css-loader',
          exclude: /(node_modules|bower_components)/
        },
        {
          test: /\.txt$/,
          loader: 'raw-loader',
          exclude: /(node_modules|bower_components)/
        },
        {
          test: /\.styl$/, 
          loader: 'raw-loader',
          exclude: /(node_modules|bower_components)/
        }
      ]
    }
  };
  return webpackConfig;
};
