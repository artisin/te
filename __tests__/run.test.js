var path = require('path');


function normalizeContent(str) {
  return str.replace(/\r/g, '').trim();
}

function runTest (type, should, styl, css) {
  return describe(type, function () {
    it(should, function () {
      styl.should.equal(css);
    });
  });
}

function addTests (type) {
  var testList = require.context('./cases', true, /.txt/g).keys();
  testList.forEach(function (val) {
    val = path.normalize(val);
    var txtPath = val,
        cssPath = val.replace('txt', 'css'),
        itShould =  require('./cases/' + txtPath).toString(),
        css = normalizeContent(require('./cases/' + cssPath).toString());
    //Stylus
    var stylPath = val.replace('txt', 'styl');
    stylPath = '/COMPILED/' + stylPath.replace(/\.styl$/, '.compiled.css');
    var styl = normalizeContent(require('./cases' + stylPath).toString());
    return runTest(type, itShould, styl, css);
  });
}
addTests('stylus');