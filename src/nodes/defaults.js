var stylus     = require('stylus'),
    Nodes      = stylus.nodes,
    Utils      = stylus.utils,
    helpers    = require('./helpers'),
    _          = require('lodash'),
    teFlow     = require('te-flow'),
    deepExtend = require('deep-extend');


exports.style = function (data) {
  //remove stlye
  data = _.omit(data, 'style');
  var res = _.defaults(data, {
    height: '60px',
    border: 'initial'
  });

  var lineHeight = res.height;
  //TODO fuck line-height??
  // if (res.border !== 'initial' && res.border[0]) {
  //   lineHeight = 'calc(' + res.height + ' - ' + (res.border[0] * 2) + ')';
  // }
  // if (data['border-width']) {
  //   debugger
  // }

  res = _.defaults(data, {
    position: 'relative',
    width: '200px',
    margin: '0rem',
    padding: '0rem',
    background: '#2196f3',
    color: '#fff',
    'border-radius': '4px',
    'box-shadow': 'initial',
    'font-size': '1rem',
    'font-weight': 'initial',
    'font-family': 'initial',
    'text-align': 'center',
    'vertical-align': 'middle',
    'text-decoration': 'none',
    'line-height': lineHeight,
    cursor: 'pointer',
    overflow: 'hidden',
    'z-index': 0,
    transform: 'translateZ(0)'
  });

  if (data['font-smoothing'] !== false) {
    res = _.defaults(res, {
      '-webkit-backface-visibility': 'hidden',
      'backface-visibility': 'hidden',
      '-moz-osx-font-smoothing': 'grayscale'
    });
  }

  return res;
};