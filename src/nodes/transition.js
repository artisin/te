var stylus = require('stylus'),
    Nodes  = stylus.nodes,
    Utils  = stylus.utils,
    helpers = require('./helpers'),
    _      = require('lodash'),
    teFlow = require('te-flow'),
    deepExtend = require('deep-extend');



var transition = {
  init: function (stylusRef, selector, data, option) {
    var self = this;
    this._H = helpers;
    // args
    this.stylusRef = stylusRef;
    this.data = data;
    this.selector = selector;
    this.option = option;

    debugger

    this.tmpl = {
      property: [],
      duration: [],
      ease: [],
      delay: []
    };

    this.stateList = ['hover', 'active', 'focus', 'visited', 'link', 'customState'];
    this.omitList = ['anim', 'animation'];

    //lookup
    //this.ref.lookup('$defaultTransVal')

    //AutoGen Gate, no state will be passed
    if (!this.options.state) {
      // debugger;
      //to be returned
      // debugger
      return self.generate({autoGen: true});

    }

    debugger;

    // return utils.coerceObject(this.transRtn, true);
  },
  transExtractor: function(functionOption, stateData, defaultOption) {
    var self = this;
    // debugger

    /*
    HELPERS
     */
    /**
     * Pushes properties into the tnRef from the specified options
     * and or the default options
     * @param  {num} opt      prop index to be pushed
     * @param  {num} optIndex sub-cycle index for prop push
     * @param  {obj} tnOpt    prop options to use of default to
     * @param  {obj} tnRef    ref obj that we push to
     * @param  {str} nVal     value for corrosponing shorthand
     * @return {Nothing}      -> populates tnRef
     */
    var pushPropOpt = function (tnOpt, tnRtn, opt, optIndex, nVal) {
      //get default val or rtn val specified
      var getVal = function (nType) {
        optIndex = _.isNumber(optIndex) ? optIndex : opt;
        //set default if not specifed or default key word
        if (_.isUndefined(tnOpt[nType]) || nVal === 'default') {
          return tnOpt._default[nType];
        }else if (tnOpt[nType] && tnOpt[nType][optIndex] === 'default') {
          return tnOpt._default[nType];
        }
        //string just set otherwise it will index str
        if (_.isString(tnOpt[nType])) {
          return tnOpt[nType];
        }
        return _.isUndefined(nVal) || _.isNull(nVal)
        //find corresponding index if multi otherwise default
        ? tnOpt[nType][optIndex] || tnOpt[nType][0] || tnOpt._default[nType]
        : nVal;
      };

      // debugger
      var key;
      if (opt === 0) {
        //duration
        key = 'duration';
        tnRtn.duration.push(getVal('duration'));
      }else if (opt === 1) {
        //ease
        key = 'ease';
        tnRtn.ease.push(getVal('ease'));
      }else if (opt === 2) {
        // debugger
        //delay
        key = 'delay';
        tnRtn.delay.push(getVal('delay'));
      }

      //TODO make sure return jive rather then global push
      // debugger
      return {
        key: key,
        tnRtn: tnRtn[key]
      };
    };


    /*
    FUNKS
     */


    //TODO DOCs
    var configOption = function(tnRtn, tnProp, tnOpt, tnDef, tnRoot, fnOpt) {
      //HERE !!!!!!!!!!!!!!!!!!
      debugger;
      /**
       * Cycles through shorthand options to be pushed to tnRtn
       */
      var convertShorthand = function (shorthand) {
        _.forEach(shorthand, function (val, key) {
          //property
          tnRtn.property.push(key);
          //cycle through and assign
          for (let i = 0; i < 3; i++) {
            let res = pushPropOpt(tnOpt, tnRtn, i, null, val[i]);
            tnRtn[res.key] = res.tnRtn;
          }
        });
      };
      /**
       * Pushes specified props from propery option to tnRtn
       */
      var assignProps = function (props) {
        for (let i = 0; i < props.length; i++) {
          tnRtn.property.push(props[i]);
          //cycle through options
          for (let j = 0; j < 3; j++) {
            let res = pushPropOpt(tnOpt, tnRtn, j, i);
            tnRtn[res.key] = res.tnRtn;
          }
        }
      };
      /*
      If Gate
       */
      if (tnOpt.shorthand) {
        convertShorthand(tnOpt.shorthand);
      }
      if (tnOpt.property) {
        assignProps(tnOpt.property);
      }
      // debugger
      return {
        fnOpt: fnOpt,
        tnProp: tnProp,
        tnOpt: tnOpt,
        tnRtn: tnRtn
      };
    };



    /**
     * Generates transition props for props who transtion was
     * not defined.
     */
    var genPropOpts = function (fnOpt, tnProp, tnOpt, tnRtn) {
      // debugger
      //add props to be reffrecned latter
      tnOpt._props = self._H.getAttributes(tnProp);
      //Find out the properties we need to gen for.
      //Only gen for properties that are not specified
      var propsToGen = _.filter(tnOpt._props, function (n) {
        return _.indexOf(tnRtn.property, n) === -1;
      });
      /**
       * Pushes specified props from propery option to tnRtn
       */
      var assignProps = function (props) {
        for (let i = 0; i < props.length; i++) {
          tnRtn.property.push(props[i]);
          for (let j = 0; j < 3; j++) {
            let res = pushPropOpt(tnOpt, tnRtn, j, null);
            tnRtn[res.key] = res.tnRtn;
          }
        }
      };
      /*
      If Gate
       */
      if (propsToGen.length) {
        assignProps(_.unique(propsToGen));
      }
      // debugger
      return {
        fnOpt: fnOpt,
        tnProp: tnProp,
        tnOpt: tnOpt,
        tnRtn: tnRtn
      };
    };




    var transControl = function (fnOpt, tnData, tnDefault) {
      // debugger
      return _.reduce(_.keys(tnData), function (prv, targetKey) {
        var target = tnData[targetKey];
        // var defaultOpt = target.default;
        var root = target.root;
        target = _.omit(target, 'root');
        // debugger
        target = _.reduce(_.keys(target), function (_prv, _curKey) {
          debugger
          var tnProp = self._H.getAttributes(target[_curKey]);
          var tnOpt = target[_curKey].option ? target[_curKey].option : {};
          var subTarget = teFlow(
            {
              _args: {
                tnRtn: _.cloneDeep({}, self.tmpl),
                tnProp: tnProp,
                tnOpt: tnOpt,
                tnDef: tnDefault,
                tnRoot: root,
                fnOpt: fnOpt
              }
            },
            configOption
          );

        }, {});
        
      }, {});
    }

    // debugger;
    // var stateOpts = configOption.apply(null, stateObj);
    // pushProps.apply(null, genPropOpts.apply(null, stateOpts));
    // self.composeReturn();

    //funk return
    return teFlow(
      {
        _args: {
          //TODO should I merege here???
          fnOpt: _.defaults(functionOption, {}),
          tnData: stateData,
          tnOpt: defaultOption
        }
      },
      transControl,
      {
        return: function (fnOpt, tnProp, tnOpt, tnRtn) {
          debugger
          //What will be return from this funk the sole reson
          //I do this is to return an object not a array with
          //an object inside of it.
          return {
            fnOpt: fnOpt,
            tnProp: tnProp,
            tnOpt: tnOpt,
            tnRtn: tnRtn
          };
        }
      }
    );
  },


  generate: function (fnOption) {
    // debugger;
    var self = this;

    /**
     * Cycles though obj and finds the states that we
     * need to work with.
     * @param  {Function} done cb
     * @param  {ojb}      data - of current obj scope
     * @return {obj}      the states
     */
    var findStates = function () {
      var data = self.data;
      //TODO global trans
      //treat like state??????
      var trans = self._L.findTransObj(data);
      if (trans) {
        //defined global trans
        debugger;
      }
      var states = _.filter(_.keys(data), function (val) {
        return _.indexOf(self.stateList, val) >= 0 || val.match(/customState/);
      });
      return {
        _objKeep: false,
        states: states
      };
    };

    /**
     * cycle through states and creates obj with states as sub-keys
     * and snags the default options for latter ref. Also the first
     * gate to pass for states such as link
     * @param  {arr} states - found states in obj
     * @return {obj}        - with the key being the state {hover: ..}
     */
    var configStates = function (states) {
      //configs args for to be state transitions
      // debugger;
      return _.reduce(states, function (prv, state) {
        // debugger
        var stateData = _.omit(self.data[state], 'anim', 'animation');
        //check to see if nonDefault extraction for transtions
        var nonDefault = _.indexOf(['link', 'visited', 'customState'], state) >= 0 || state.match(/customState/);
        var stateTrans = self._L.findTransObj(stateData, true);
        //determins if we extract of not
        var extract = (!nonDefault && stateTrans !== false) || (nonDefault && stateTrans);
        if (extract) {
          //TODO state specific
          var defaultOpt = self._H.toObject.call(self, self.stylusRef.lookup('$defaultTransVal'));
          //create object with state as sub key
          prv[state] = {
            stateData: stateData,
            defaultOpt: defaultOpt
          };
          return prv;
        }
        return prv;
      }, {});
    };

    /**
     * This orginizes the state object a bit and seperates it into
     * three sub-trans cats and adds a global root obj as well.
     * @return {obj}
     */
    var organizeStates = function () {
      // debugger
      return _.reduce([...arguments], function (prv, state) {
        // debugger
        var stateKey = _.keys(state)[0];
        state = state[stateKey];
        var stateData = state.stateData;
        var hasTransObj = self._L.findTransObj(stateData);
        stateData = hasTransObj ? hasTransObj : {trans: stateData};
        //global data
        var globalObj = self._L.findGlobal(stateData);
        var globalData = _.omit(globalObj, 'option');
        var globalOpt = globalObj.option ? globalObj.option : {};
        //remvoe global
        stateData = _.omit(stateData, 'global');

        // debugger
        //Main opts which are handled as seperate
        //entites, or atleast thats what I plan
        var main = {
              default: {}
            },
            media = {},
            applyTo = {};

        // debugger
        //Seperates into respecitve object
        _.forEach(stateData, function (val, key) {
          // debugger
          if (val.option) {
            var opt = val.option;
            //just creates an obj key pair
            var toObj = function (_key, _val) {
              var obj = {};
              obj[_key] = _val;
              return obj;
            };
            //configs, defautlts, and added root obj if need
            var configObj = function (target) {
              // debugger
              //add root to target if present to be
              //populated dynamically
              if (opt.root) {
                target.root = _.cloneDeep({}, self.tmpl);
              }
              return deepExtend(target, toObj(key, _.defaultsDeep(val, globalData)));
            };
            //primary opt gate
            if (opt.media) {
              media = configObj(media);
            }else if (opt.applyTo) {
              applyTo = configObj(applyTo);
            }else {
              main = configObj(main);
            }
          }else {
            //merge to main since no opts
            main.default = deepExtend(main.default, _.defaultsDeep(val, globalData));
          }
        });

        //check to see if empty if no adds obj
        //to res to be returned
        var filterRes = function () {
          var res = {};
          if (!_.isEmpty(main)) { res.main = main; }
          if (!_.isEmpty(media)) { res.media = media; }
          if (!_.isEmpty(applyTo)) { res.applyTo = applyTo; }
          return {
            stateData: res,
            defaultOpt: _.merge(state.defaultOpt, globalOpt)
          };
        };
        
        // debugger
        prv[stateKey] = filterRes();
        return prv;
      }, {});
    };

    /**
     * The main brain.
     * @return {[type]} [description]
     */
    var extractStates = function () {
      // debugger
      return _.reduce([...arguments], function (prv, state) {

        //type of state
        var stateKey = _.keys(state)[0];
        state = state[stateKey];

        // debugger
        prv[stateKey] = self.transExtractor(fnOption,
                                            state.stateData,
                                            state.defaultOpt);

        // self.transExtractor(subProp, subOpt, {autoGen: true});

        // // debugger
        // var stateObj    = state.stateObj,
        //     defaultOpts = state.defaultOpt;

        //formate to keep all the same
        // stateObj = stateTransObj ? _.omit(stateTransObj, 'global') : {main: stateObj};
        
        // // debugger
        // //Maps out return, if single trans it will have a single key of main
        // var res = _.reduce(_.keys(stateObj), function (_prv, subKey) {
        //   // debugger
        //   var subProp = stateObj[subKey],
        //       subOpt  = subProp.option || {};
        //   //remove option from props
        //   subProp = _.omit(subProp, 'option');
        //   //add reffrence to default vals
        //   subOpt._default = stateOpt;

        //   // debugger
        //   //overides
        //   var autoGenOvr = subOpt.autoGen === false;
        //   if (!autoGenOvr && self._L.findTransObj(subProp, true) !== false) {
        //     var extract = !subOpt ? true : subOpt.root || !subOpt.applyTo;
        //     if (extract) {
        //       debugger;
        //       var _res = self.transExtractor(subProp, subOpt, {autoGen: true});
        //       _prv[subKey] = _res;
        //       return _prv;
        //     }
        //     //Did not pass extract gate
        //     return _prv;
        //   }
        // }, {});

        // // debugger
        // prv[arrIndex] = {};
        // prv[arrIndex][stateKey] = res;
        return prv;
      }, {});
    };

    //for composing state for main return
    var composeStates = function () {
      debugger

      var states = [...arguments];
      var hmm = states[0].active
      var work = deepExtend({}, hmm.one, hmm.two)
      var res = _.reduce(states, function (prv, state) {
        debugger
        var stateKey = _.keys(state)[0];
        prv[stateKey] = self.composeReturn(state[stateKey]);
        return prv;
      }, {});

      debugger;
    };



    //!!!       self.composeReturn
    return teFlow(
      {
        _objKeep: true
      },
      findStates,
      configStates,
      organizeStates,
      extractStates,
      composeStates
    );

  },
  /*--------------------------------------------------------------------------*/
  /*--------------------------------------------------------------------------*/
  // Compose Return
  /*--------------------------------------------------------------------------*/
  composeReturn: function (stateData) {
    var self = this;
    // var [type] = self.options.type;
    debugger

    var assignTrans = function (trans) {
      var formated = {};
      formated['transition-property'] = trans.property;
      formated['transition-duration'] = trans.duration;
      formated['transition-timing-function'] = trans.ease;
      formated['transition-delay'] = trans.delay;
      formated.pseudo = trans.pseudo ? true : null;
      formated.autoGen = trans.autoGen === false ? false : null;
      formated.media = trans.media ? trans.media : null;
      formated.applyTo = trans.applyTo ? trans.applyTo : null;
      // debugger;
      return formated;
    };

    var composeTrans = function (stateData, rtnData) {
      debugger
      // var rtnObj  = rtnData.rtnObj,
      //     type    = rtnData.type,
      //     state   = stateData.state;

      var res = {};
      //media
      if (stateData.media) {
        var media = stateData.media;
      }
      //root
      if (stateData.root) {
        var root = stateData.root;
      }
      
      res = _.reduce(_.keys(_.omit(stateData, 'media', 'root', 'apply')), function (prv, cur) {
        var hmm = stateData[cur];
        debugger
        //create main if not present
        if (!prv.main) {
          prv['main'] = {};
        }

        prv['main'] = _.isEmpty(prv['main'])
                      ? assignTrans(hmm)
                      : 'hmm'
      }, res);

      debugger
    };




    
    // toStylus(composeMain(composeRtnObj()));
    return teFlow(
      {
        _args: {
          stateData: stateData,
          rtnObj: function () {
            var type = self.options.type;
            var rtnObj = {};
            rtnObj[type] = {};
            return {
              rtnObj: rtnObj,
              type: type
            };
          }
        }
      },
      composeTrans
    );


  },
  //Helper Utilites
  _L: {
    findTransObj: function (data, rtnNull) {
      // debugger
      if (!data) {
        return rtnNull ? null : false;
      }
      //trans key
      if (data.trans) {
        return data.trans;
      }
      //transintion key
      if (data.transition) {
        return data.transition;
      }
      //make sure user not overiding
      if (data.option) {
        var opt = data.option;
        return opt.trans ? opt.trans : opt.transition;
      }
      return rtnNull ? null : false;
    },
    //TODO _H
    findOpt: function (data, opt) {
      return data.option ? data.option[opt] : undefined;
    },
    //TODO _H
    mergeGlobal: function (data, global) {
      return global.length ? _.merge(global, data) : data;
    },
    findGlobal: function (data) {
      return data.global ? data.global : {};
    }
  }
};






exports.create = function (stylusRef, selector, data, options) {
  debugger;
  return Object.create(transition).init(stylusRef, data, selector, options);
};