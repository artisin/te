var stylus = require('stylus'),
    Nodes  = stylus.nodes,
    Utils  = stylus.utils,
    _      = require('lodash');


var toObject = function(obj) {
  obj = obj.length ? obj : obj.first;
  if (!obj.length) {
    if (obj.nodeName === 'call') {
      // debugger
      return {literal: obj.toString()};
    }
    //debugger
    return {};
  }
  return _.reduce(obj.vals, function (res, n, key) {
    var configNode = function (nVal) {
      var nodeName = nVal.first.nodeName;
      // debugger
      if (nodeName === 'object') {
        // debugger;
        return toObject.call(this, nVal.first);
      }else if (nodeName === 'rgba') {
        //TODO hsla
        return nVal.first.toString();
      }else if (nodeName === 'unit') {
        // debugger
        return nVal.first.toString();
        // return {val: nVal.first.val, type: nVal.first.type};
      }else if (nodeName === 'ident') {
        //Bold val for fw
        if (nVal.first.val.isNull) {
          // return {indent: nVal.first.toString()};
          return nVal.first.toString();
        }
        debugger;
      }else if (nodeName === 'boolean') {
        return nVal.first.val;
      }else if (nodeName === 'string') {
        return nVal.first.val;
      }else if (nodeName === 'literal') {
        //refrance of another var?
        return toObject.call(this, this.stylusRef.lookup(nVal.first.val));
      }else if (nodeName === 'call') {
        //like linerat grafient
        return nVal.first.toString();
      }
      //default, should not get here!
      debugger;
      return nVal.first.toString();
    };
    var len = n.nodes.length;
    //sinlge val
    if (len === 1) {
      res[key] = configNode.call(this, n.nodes[0]);
    }else {
      //multi val, build arr
      res[key] = [];
      var resObj = res[key];
      for (var i = 0; i < len; i++) {
        resObj.push(configNode.call(this, n.nodes[i]));
      }
    }
    return res;
  }, {}, this);
};
exports.toObject = toObject;


var toStylus = function (rtObj) {
  // debugger
  var res = _.reduce(_.keys(rtObj), function (prv, key) {
    var val = rtObj[key];
    // debugger
    if (_.isString(val)) {
      val = new Nodes.Literal(val);
    } else if (_.isNumber(val)) {
      val = new Nodes.Unit(val);
    }else if (_.isArray(val)) {
      // debugger
      val = new Nodes.Literal(val.join(' '));
    } else {
      debugger
    }

    prv[key] = val;
    // debugger
    return prv;
  }, {});

  // debugger
  return Utils.coerceObject(res, true);
};
exports.toStylus = toStylus;






//TODO do I need
var getAttributes = function(propList) {
  var omit = ['before', 'after', 'applyTo', 'selector', 'state', 'element', 'isRoot',
              'hover', 'active', 'visited', 'focus', 'link', 'customState',
              'anim', 'animation', 'transition', 'override', 'cache', '$$', 'import',
              'duration', 'delay', 'ease', 'property', 'root', 'customeElement',
              'option', 'addClass', 'component', 'pseudo', 'error', 'global',
              'type', 'autoGen', 'style', 'media'];
  return _.pull(_.keys(propList), _.forEach(omit, function (val) {
    return val;
  }));
};
exports.getAttributes = getAttributes;



var isAllObjs = function(data) {
  var omitList = ['option', 'global', 'shorthand', 'timeline'];
  // add on any args as omit
  var addOn = _.rest(arguments)[0];
  if (addOn) {
    for (var i = addOn.length - 1; i >= 0; i--) {
      omitList.push(addOn[i]);
    }
  }
  return data ? _.every(data, function (val) {
    return _.every(val, Object);
  }) : false;
};





exports.str = function(arg, keys){
  console.log('||||||||||||===========================>')
  console.log(arg)
  console.log(arg.toString())
  if (keys) {
    _.forEach(arg.vals, function(n, key) {
      console.log('Key ----> ' + key)
      console.log(n.toString())
      // debugger
      // console.log(key.vals.toBlock());
    });
  }
};




exports.getAttributes = function(propList){
  var vals = propList.vals ? propList.vals : false;
  if (!vals) {
    return new nodes.Expression();
  }
  var omitList = ['before', 'after', 'applyTo', 'selector', 'state', 'element', 'isRoot', 'hover', 'active', 'visited', 'focus', 'link', 'customState', 'anim', 'animation', 'transition', 'override', 'cache', '$$', 'import', 'duration', 'delay', 'ease', 'property', 'root', 'customeElement', 'option', 'globalOption', 'addClass', 'component', 'pseudo', 'error', 'type'];
  vals = _.omit(vals, omitList);
  return utils.coerceObject(vals, true); 
};


exports.isAllObjs = function(data) {
  var omitList = ['option', 'global', 'shorthand', 'timeline'];
  debugger
  // _.rest(arguments);
  if (omitList.length) {
    debugger
  }
  data = data.vals ? data.vals : false;
  return data ? _.every(data, function (val) {
    return _.every(val.nodes, Object);
  }) : false;
};

