var stylus     = require('stylus'),
    Nodes      = stylus.nodes,
    Utils      = stylus.utils,
    helpers    = require('./helpers'),
    _          = require('lodash'),
    teFlow     = require('te-flow'),
    getDefault = require('./defaults'),
    deepExtend = require('deep-extend'),
    transition = require('./transition');









var Te = {
  init: function (stylusRef, data, selector, id) {
    var self = this;
    this.stylusRef = stylusRef;
    this._H = helpers;


    this.data = this._H.toObject.call(this, data);
    if (selector.nodeName === 'object') {
      this.selector = this._H.toObject.call(this, selector);
      this.selectorObj = true;
    } else {
      this.selector = selector.val;
      this.selectorObj = false;
    }
    this.id = id.val;
    // debugger
    // debugger
    //Check if style === true for defaults
    // debugger
    if (this.data.style) {
      this.data = getDefault.style(this.data);
    }

    //TODO default set
    if (this.data.default) {
      debugger;
    }

    // {data, selector}
    var res = this.extractor(this.selector, this.data);

    // debugger
    return Utils.coerceArray(res, true);

  },
  /*--------------------------------------------------------------------------*/
  /*--------------------------------------------------------------------------*/
  // Extractor
  /*--------------------------------------------------------------------------*/
  extractor: function (_selector, _data) {
    var self = this;
    var dataStream = [];

    var cycleExtract = function (curSelector, curData, curOption = {}) {
      //config data
      var configData = function (selector, data, option) {
        var staticArgs = {};
        var objArgs = {};
        var keys = _.keys(data);
        for (var i = 0; i < keys.length; i++) {
          var key = keys[i];
          //silly javascript types
          if (!_.isObject(data[key]) || _.isArray(data[key])) {
            staticArgs[key] = data[key];
          }else {
            objArgs[key] = data[key];
          }
        }
        return {
          selector: selector,
          staticArgs: staticArgs,
          objArgs: objArgs,
          option: option
        };
      };

      /**
       * The plan for this fn is to cycle through the various
       * components, and then merge and res of said comps
       */
      var composeData = function (selector, staticArgs, objArgs, option) {
        if (!_.keys(objArgs).length) {
          return {
            selector: selector,
            staticArgs: staticArgs,
            objArgs: objArgs,
            option: option
          };
        }

        //args to work with
        // debugger;
        //effects
        var transEffect = self.transEffect(selector, objArgs, option);
        debugger
      };




      return teFlow(
        {
          _args: {
            selector: curSelector,
            data: curData,
            option: curOption
          }
        },
        configData,
        composeData,
        {
          return: function (selector, staticArgs, objArgs, optionArgs) {
            // debugger
            dataStream.push(Utils.coerceObject({
              selector: self.selectorObj
                        ? self._H.toStylus(selector)
                        : new Nodes.Literal(selector),
              data: self._H.toStylus(staticArgs)
            }, true));
            return !objArgs.length
                   ? dataStream
                   : cycleExtract(selector, objArgs);
          }
        }
      );
    };


    //main call
    return cycleExtract(_selector, _data);
  },
  /*----------------------------------------------------------------------------*/
  /*----------------------------------------------------------------------------*/
  // Trans Effect
  /*----------------------------------------------------------------------------*/
  transEffect: function (selector, data, option) {
    debugger
    var transStream = [];
    var animStream = [];


    //check for global trans
    var globalEffects = function () {
      var globalTrans = data.trans ? data.trans : data.transition;
      if (globalTrans) {
        var res = transition.create(globalTrans, {});
      }

      var globalAnim = data.anim ? data.anim : data.animation;
      if (globalAnim) {
        debugger;
      }
    };

    var stateEffects = function () {
      //cycle
    };

    var cusomEffects = function () {
      //cycle
    };


    return teFlow(
      globalEffects,
      stateEffects,
      cusomEffects,
      {
        return: function () {
          debugger
        }
      }
    );
  }
};


exports.createStyle = function (data, selector, id) {
  var stylusRef = this;
  // debugger;
  return Object.create(Te).init(stylusRef, data, selector, id);
};

