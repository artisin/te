/*!
 * te
 * Copyright (c) 2015 Te
 * MIT Licensed
 */

/**
 * Module dependencies.
 */

var path = require('path'),
    helper = require('./nodes/helpers'),
    trans = require('./nodes/transition'),
    te = require('./nodes/index');

// exports.version = require(path.join(__dirname, '../package.json')).version;
// exports.path = __dirname;

/**
 * Return the plugin callback for stylus.
 *
 * @return {Function}
 * @api public
 */


exports = module.exports = function plugin(opts) {
  var implicit = (opts && opts.implicit === false) ? false : true;
  return function(style) {

    style.include('./lib');

    if (implicit) {
        style.import('te');
    }
    // style.import(s'buttron')
    // style.define('getAttributes', helper.getAttributes);
    // style.define('isAllObjs', helper.isAllObjs);
    // debugger
    // style.define('buttronProcess', helper.buttronProcess);
    style.define('extractTe', te.createStyle);
    style.define('transExtractorJS', trans.createTrans);

    style.define('debug', helper.debug);
    style.define('str', helper.str);

  };
};
